/*
 * bibliotheek.c
 *
 *  Created on: 14 feb. 2018
 *     Authors: VersD / BroJZ
 */

#include <msp430.h>
#include "../inc/bibliotheek.h"

bool zet_klok_op_MHz(uint8_t mhz)
{
    DCOCTL = 0;

    if (CALBC1_1MHZ==0xFF || CALBC1_8MHZ==0xFF || CALBC1_12MHZ==0xFF || CALBC1_16MHZ==0xFF)
    {
        return false;
    }

    switch (mhz)
    {
    case 1:
        BCSCTL1 = CALBC1_1MHZ; // Set range
        DCOCTL = CALDCO_1MHZ;  // Set DCO step + modulation */
        break;
    case 8:
        BCSCTL1 = CALBC1_8MHZ; // Set range
        DCOCTL = CALDCO_8MHZ;  // Set DCO step + modulation */
        break;
    case 12:
        BCSCTL1 = CALBC1_12MHZ; // Set range
        DCOCTL = CALDCO_12MHZ;  // Set DCO step + modulation */
        break;
    case 16:
        BCSCTL1 = CALBC1_16MHZ; // Set range
        DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */
        break;
    default:
        return false;
    }

    return true;
}

void zet_pin_richting(uint8_t poort, uint8_t pin, Richting richting)
{
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if (richting == input)
            {
                P1DIR &= ~(1<<pin);
            }
            else if (richting == output)
            {
                P1DIR |= 1<<pin;
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if (richting == input)
            {
                P2DIR &= ~(1<<pin);
            }
            else if (richting == output)
            {
                P2DIR |= 1<<pin;
            }
        }
    }
}

void output_pin(uint8_t poort, uint8_t pin, Waarde waarde)
{
    // vul hier de benodigde code in
}

void zet_interne_weerstand(uint8_t poort, uint8_t pin, Weerstand weerstand)
{
    // vul hier de benodigde code in
}

bool input_pin(uint8_t poort, uint8_t pin)
{
    // vul hier de benodigde code in
}
